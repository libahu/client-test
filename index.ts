import { io } from 'socket.io-client';

const socket = io('https://api-dev.chainverse.org/fiat', { transports: ['websocket'] });
// client-side
socket.on('connect', () => {
	console.log(socket.id); // x8WIv7-mJelg7on_ALbx
});

socket.on('disconnect', () => {
	console.log(socket.id); // undefined
});


socket.on('connect-server', (data)=>{
	console.log(data);
    socket.emit("join_room", "0x79200b8A31ac8b2B8FC4c68a07309010a5061098");    //add address
})

socket.on("TAKER_CANCEL_ORDER", (data)=> {
    console.log(data);
})

